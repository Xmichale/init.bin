CROSS_COMPILE=arm-linux-
LD=$(CROSS_COMPILE)ld
OBJCOPY=$(CROSS_COMPILE)objcopy
OBJDUMP=$(CROSS_COMPILE)objdump
CC=$(CROSS_COMPILE)gcc
#CFLAGS:=-g
CFLAGS+=-Wall
objs := init.o
NAND_TEXT_BASE=0x00000000
NONAND_TEXT_BASE=0x40000000

.PHONY:all

all: $(objs)
	$(LD) -Ttext $(NAND_TEXT_BASE) -o init_nandelf $^
	$(LD) -Ttext $(NONAND_TEXT_BASE) -o init_elf $^
	$(OBJCOPY) -O binary -S init_nandelf init_nand.bin
	$(OBJCOPY) -O binary -S init_elf init.bin
	$(OBJDUMP) -D -m arm init_nandelf > init_nand.dis
	$(OBJDUMP) -D -m arm init_elf > init.dis

#%.o:%.c
#	$(CC) -Wall -g -c -o $@ $<

%.o:%.S lowlevel_init.h
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	@rm -f *.bin *.dis *.o init_elf  init_nandelf

