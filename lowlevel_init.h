#ifndef _LOWLEVEL_INIT_
#define _LOWLEVEL_INIT_
/* some parameters for the board */

/*
 *
 * Taken from linux/arch/arm/boot/compressed/head-s3c2410.S
 *
 * Copyright (C) 2002 Samsung Electronics SW.LEE  <hitchcar@sec.samsung.com>
 *
 */

#define  DELAY_COUNT 0xff0000

#define pWTCON      0x53000000
#define INTMSK      0x4A000008
#define INTSUBMSK   0x4A00001C
#define BWSCON      0x48000000

#define CLK_POWER_BASE	0x4C000000

#define MPLLCON		0x4C000004
#define UPLLCON		0x4C000008
#define CLKDIVN		0x4C000014

#define MDIV_405MHZ		(0x7f << 12)
#define PSDIV_405MHZ	(0x21)

#define BWSCON	0x48000000

/* BWSCON */
#define DW8			(0x0)
#define DW16		(0x1)
#define DW32		(0x2)
#define WAIT		(0x1<<2)
#define UBLB		(0x1<<3)

/* bus width for BANK1 ~ BANK7  */
#define B1_BWSCON		(DW16)
#define B2_BWSCON		(DW16)
#define B3_BWSCON		(DW16 + WAIT + UBLB)
#define B4_BWSCON		(DW16)
#define B5_BWSCON		(DW16)
#define B6_BWSCON		(DW32)
#define B7_BWSCON		(DW32)

#define BANK_TACS_0CLK 0x0
#define BANK_TACS_1CLK 0x1
#define BANK_TACS_2CLK 0x2
#define BANK_TACS_4CLK 0x3

#define BANK_TCOS_0CLK 0x0
#define BANK_TCOS_1CLK 0x1
#define BANK_TCOS_2CLK 0x2
#define BANK_TCOS_4CLK 0x3

#define BANK_TACC_1CLK 0x0
#define BANK_TACC_2CLK 0x1
#define BANK_TACC_3CLK 0x2
#define BANK_TACC_4CLK 0x3
#define BANK_TACC_6CLK 0x4
#define BANK_TACC_8CLK 0x5
#define BANK_TACC_10CLK 0x6
#define BANK_TACC_14CLK 0x7

#define BANK_TCOH_0CLK 0x0
#define BANK_TCOH_1CLK 0x1
#define BANK_TCOH_2CLK 0x2
#define BANK_TCOH_4CLK 0x3

#define BANK_TACH_0CLK 0x0
#define BANK_TACH_1CLK 0x1
#define BANK_TACH_2CLK 0x2
#define BANK_TACH_4CLK 0x3

#define BANK_TACP_2CLK 0x0
#define BANK_TACP_3CLK 0x1
#define BANK_TACP_4CLK 0x2
#define BANK_TACP_6CLK 0x3

#define BANK_PMC_NORMAL 0x0
#define BANK_PMC_1DATA 0x0
#define BANK_PMC_4DATA 0x1
#define BANK_PMC_8DATA 0x2
#define BANK_PMC_16DATA 0x3

/* BANK0CON */
#define B0_Tacs			BANK_TACS_0CLK	
#define B0_Tcos			BANK_TCOS_0CLK
#define B0_Tacc			BANK_TACC_14CLK
#define B0_Tcoh			BANK_TCOH_0CLK
#define B0_Tah			BANK_TACH_0CLK
#define B0_Tacp			BANK_TACP_2CLK
#define B0_PMC			BANK_PMC_NORMAL

/* BANK1CON */
#define B1_Tacs			BANK_TACS_0CLK	
#define B1_Tcos			BANK_TCOS_0CLK
#define B1_Tacc			BANK_TACC_14CLK
#define B1_Tcoh			BANK_TCOH_0CLK
#define B1_Tah			BANK_TACH_0CLK
#define B1_Tacp			BANK_TACP_2CLK
#define B1_PMC			BANK_PMC_NORMAL

/* BANK2CON */
#define B2_Tacs			BANK_TACS_0CLK	
#define B2_Tcos			BANK_TCOS_0CLK
#define B2_Tacc			BANK_TACC_14CLK
#define B2_Tcoh			BANK_TCOH_0CLK
#define B2_Tah			BANK_TACH_0CLK
#define B2_Tacp			BANK_TACP_2CLK
#define B2_PMC			BANK_PMC_NORMAL

/* BANK3CON */
#define B3_Tacs			BANK_TACS_0CLK	
#define B3_Tcos			BANK_TCOS_4CLK
#define B3_Tacc			BANK_TACC_14CLK
#define B3_Tcoh			BANK_TCOH_1CLK
#define B3_Tah			BANK_TACH_0CLK
#define B3_Tacp			BANK_TACP_6CLK
#define B3_PMC			BANK_PMC_NORMAL

/* BANK4CON */
#define B4_Tacs			BANK_TACS_0CLK	
#define B4_Tcos			BANK_TCOS_0CLK
#define B4_Tacc			BANK_TACC_14CLK
#define B4_Tcoh			BANK_TCOH_0CLK
#define B4_Tah			BANK_TACH_0CLK
#define B4_Tacp			BANK_TACP_2CLK
#define B4_PMC			BANK_PMC_NORMAL

/* BANK5CON */
#define B5_Tacs			BANK_TACS_0CLK	
#define B5_Tcos			BANK_TCOS_0CLK
#define B5_Tacc			BANK_TACC_14CLK
#define B5_Tcoh			BANK_TCOH_0CLK
#define B5_Tah			BANK_TACH_0CLK
#define B5_Tacp			BANK_TACP_2CLK
#define B5_PMC			BANK_PMC_NORMAL

/* BANK6CON */
#define B6_MT			0x3	/* SDRAM */
#define B6_Trcd			0x1
#define B6_SCAN			0x1	/* 9bit */

/* BANK7CON */
#define B7_MT			0x3	/* SDRAM */
#define B7_Trcd			0x1	/* 3clk */
#define B7_SCAN			0x1	/* 9bit */

/* 
 * REFRESH parameter
 */
#define REFEN			0x1		/* Refresh enable */
#define TREFMD			0x0		/* CBR(CAS before RAS)/Auto refresh */
#define Trp				0x0		/* 2clk */
#define Trc				0x3		/* 7clk */
#define Tchr			0x2		/* 3clk */
#define REFCNT			1269	/* period=7.8us, HCLK=100Mhz, (2048+1-15.6*60) */
/**************************************/


#endif
