## 工程生成文件
### Nand flash启动方式:
init_nand.bin   用于烧录的二进制文件  
init_nand.dis   汇编代码  

### Nor flash启动方式:
init.bin        用于烧录的二进制文件  
init.dis        反汇编代码  

### J-link烧录步骤:
### 1.搭建好软硬件环境
假定烧录文件存放于E盘,将J-link连接mini2440开发板于电脑,串口也要使用USB转串口线连接电脑
电脑端打开:
1. J-Link ARM ，当前使用的版本为V4.50
2. Windows下可以是超级终端/SecureCRT等,linux系统下可以是minicom/ckermit等

在J-link ARM终端下,设置时钟频率:  
`speed 12000`  

### 2.将init.bin加载到内存当中
#### 2.1 Nand flash启动方式,J-Link ARM命令行输入命令:
```
r                               重启开发板
h                               让PCU停止执行命令
loadbin E:\init_nand.bin 0x0    加载二进制文件到内存0x0地址当中
setpc 0                         设置PC值为0
g                               让CPU重新执行指令,此时PC为0,则执行地址0的命令,即init.bin的第一条指令
```

####2.2 Nor flash启动方式,J-Link ARM命令行输入命令:
```
r                                       重启开发板
h                                       让PCU停止执行命令
loadbin E:\init.bin 0x40000000          加载二进制文件到内存0x40000000地址当中
setpc 0x40000000                        设置PC值为0x40000000
g                                       让CPU重新执行指令,此时PC为0x40000000,则执行地址0x40000000的命令,即init.bin的第一条指令

```

此时可以看到mini2440开发板的流水灯在不断闪烁

###3.加载u-boot到内存中,J-Link ARM命令行输入命令:
```
h                                   让PCU停止执行命令
loadbin E:\u-boot.bin 0x33f80000    0x33f80000 这个地址为0x34000000(64MiB) - 80000(512KiB),即此时u-boot.bin最大可是512kB
setpc 0x33f80000                    设置PC值为0x33f80000
g                                   让CPU重新执行指令
```

此时可以看到终端有打印u-boot启动信息,但要明白,此时的u-boot仅仅是加载到内存当中,并没有写入到Flash中,掉电后就一切白干了。所以此时u-boot必须有写flash功能,然后将真正需要烧录的u-boot写到flash中,而通过何种方式写到flash中,可以根据实际情况而定。
